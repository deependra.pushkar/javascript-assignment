const URL = 'https://api.github.com/search/repositories?q=node'

const fetch = require("node-fetch");

async function getUrl(URL) {
	let res = await fetch(URL);

	if (!res.ok) {
		throw new Error('error => ${res.status}');
	} 
	else {
		return await res.json();
	}
}

const docs = getUrl(URL);
docs.then((doc) => {
    var repos = doc.items
	var output = repos.filter(repo => repo.full_name == 'nodejs/node')[0]
    //console.log(output)
    const owner = getUrl(output.owner.url).then((data) => {return data});
   // console.log(owner)
	const branches = getUrl(output.branches_url.split("{")[0]).then((data) => {return data});


	Promise.all([output, owner, branches]).then(values => {
		let output = values[0]
		let owner = values[1]
		let branches = values[2]


		console.log({
				"name": output.name,
				"full_name": output.full_name,
				"private": output.private,
				"owner": {
					"login": output.owner.login,
					"name": owner.name,
					"followersCount": owner.followers,
					"followingCount": owner.following
				},
				"licenseName": output.license.name,
				"score": output.score,
				"numberOfBranch": branches.length
			})
	})	
})